#include <stdio.h>
#include <math.h>
#include <quadmath.h>

typedef __float128 prec_t;

const prec_t k0 = (prec_t)8589934592.0; //8987551787.3681764L;
const prec_t mass = (prec_t)1.0 / (prec_t)512.0;
const prec_t charge = (prec_t)1.0;
const prec_t step = (prec_t)1.0 / ((prec_t)1024.0 * (prec_t)1024.0 * (prec_t)1024.0 /** (prec_t)1024.0*/);

struct point {
	prec_t x, y;
};

struct vector {
	prec_t x, y;
};

struct particle {
	struct point p;
	struct vector v;//Speed
	struct vector f;//force
	prec_t m;	//mass
	prec_t q;	//charge
};

void init_particle(struct particle *pn, prec_t x, prec_t y, prec_t q)
{
	pn->p.x = x;
	pn->p.y = y;
	pn->v.x = (prec_t)0.0;
	pn->v.y = (prec_t)0.0;
	pn->m = mass;
	pn->q = q;
}

void _calc_force(struct particle *p1, struct particle *p2)
{
	prec_t dx, dy, d2, d, f;

	dx = p1->p.x - p2->p.x;
	dy = p1->p.y - p2->p.y;
	d2 = dx * dx + dy * dy;

	f = (k0 * p1->q * p2->q) / d2;

	d = sqrtq(d2);

	p1->f.x += (f * dx) / d;
	p1->f.y += (f * dy) / d;
}


void calc_force(struct particle *pl, int idx, int size)
{
	struct particle *p = &(pl[idx]);

	p->f.x = (prec_t)0.0;
	p->f.y = (prec_t)0.0;

	for(int i = 0; i < size ; i++) {
		if (i == idx)
			continue;
		_calc_force(p, &(pl[i]));
	}
}

void calc_movement(struct particle *p, prec_t step)
{
	struct vector a;

	a.x = p->f.x / p->m;
	a.y = p->f.y / p->m;

	/* For very small dt, consider it a uniformly accelerated motion */
	p->p.x += p->v.x * step + (a.x * step * step) / (prec_t)2.0;
	p->p.y += p->v.y * step + (a.y * step * step) / (prec_t)2.0;

	p->v.x += a.x * step;
	p->v.y += a.y * step;
}

void print_particle(struct particle *pl, int i)
{
	struct particle *p = &(pl[i]);
	printf("Particle %d: p = {%Qf, %Qf}\n", i, p->p.x, p->p.y);
}

int main(void)
{
	struct particle pl[5];
	int size = sizeof(pl) / sizeof(pl[0]);

	init_particle(&(pl[0]),  0.0,  0.0, -1.0);
	init_particle(&(pl[1]), -100.0, -100.0,  1.0);
	init_particle(&(pl[2]),  100.0,  100.0,  1.0);
	init_particle(&(pl[3]), -100.0,  100.0,  1.0);
	init_particle(&(pl[4]),  100.0, -100.0,  1.0);

	for(int i = 0; i < size; i++)
		print_particle(pl, i);

	for(int j = 0; j < 9000000; j++) {
		for(int i = 0; i < size; i++) {
			calc_force(pl, i, size);
			calc_movement(&(pl[i]), step);

			if (j % 1024 == 0)
				print_particle(pl, i);
		}
	}
}


