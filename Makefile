CFLAGS = -lm -lquadmath

all: *.o
	$(CC) $(CFLAGS) -o partsim *.o

clean:
	rm -f *.o
